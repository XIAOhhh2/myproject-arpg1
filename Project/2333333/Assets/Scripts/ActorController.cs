using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using TMPro;
using UnityEngine;

public class ActorController : MonoBehaviour
{
    public GameObject model;
    public CameraController cameraController;
    public IUserInput pi;
    public float walkSpeed = 1.4f;
    public float runMultiplier = 2.0f;
    public float jumpVelocity = 5.0f;
    public float rollVelocity = 1.0f;

    public PhysicMaterial friction1;
    public PhysicMaterial friction0;

    public Animator anim;
    public Rigidbody rigid;
    private Vector3 movingVec;
    private Vector3 thrustVec;
    private bool canAttack = true;
    private bool lockMove = false;
    private bool trackDirection = false;
    private CapsuleCollider col;
    private Vector3 deltaPosition;
    private float runAttackTime = 0f;
    public bool canRunAttack�@= false;

    public bool leftIsShu = true;
    public GameObject defenceWall;
    [HideInInspector]
    public BattleManager bm;


    // Start is called before the first frame update
    void Awake()
    {

        IUserInput[] inputs = GetComponents<IUserInput>();
        foreach (var input in inputs)
        {
            if(input.enabled == true)
            {
                pi = input;
                break;
            }
        }
        anim = model.GetComponent<Animator>();
        rigid = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();

        //anim.SetLayerWeight(anim.GetLayerIndex("kunai"), 0f);
        DWdown();

        GameObject sensor = transform.Find("Sensor").gameObject;
        bm = sensor.GetComponent<BattleManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (pi.lockon)
        {
            cameraController.LockUnlock();
        }

        if (cameraController.lockState == false)
        {
            if (pi.run)
            {
                anim.SetFloat("walk", pi.Dmag * Mathf.Lerp(anim.GetFloat("walk"), 2.0f, 0.05f));
            }
            else
            {
                anim.SetFloat("walk", pi.Dmag * Mathf.Lerp(anim.GetFloat("walk"), 1.0f, 0.4f));
                anim.SetFloat("right", 0);
            }
        }
        else
        {
            Vector3 localDVec = transform.InverseTransformVector(pi.Dvec);
            anim.SetFloat("walk",localDVec.z * (pi.run? 2.0f : 1.0f));
            anim.SetFloat("right", localDVec.x * (pi.run ? 2.0f : 1.0f));
        }

        if (pi.roll || rigid.velocity.magnitude > 9f )
        {
            anim.SetTrigger("roll");
            canAttack = false;
        }
        
        if (pi.jump)
        {
            anim.SetTrigger("jump");
            canAttack = false;
        }

        if ((pi.mouse0 || pi.mouse1) && (CheckState("ground") || (CheckTag ("attackR") || CheckTag("attackL"))) && canAttack )
        {
            if (pi.mouse0)
            {
                anim.SetBool("R0L1", false);
                anim.SetTrigger("attack");
                canRunAttack = false;
            }
            else if (pi.mouse1 && !leftIsShu)
            {
                anim.SetBool("R0L1", true);
                anim.SetTrigger("attack");
                canRunAttack = false;
            }
        }
        if (anim.GetFloat("walk") > 1.9f)
         {
            runAttackTime += Time.deltaTime;
           if (runAttackTime >= 3f )
            {
                canRunAttack = true;
                
                if (pi.runAttack && CheckState("ground")  && canRunAttack)
                {
                    anim.SetTrigger("runAttack");
                    runAttackTime = 0f;
                    canRunAttack = false;
                }

            }
        }
        if ((pi.mouse0A || pi.counterBack) && (CheckState("ground") || (CheckTag("attackR") || CheckTag("attackL"))) && canAttack && canRunAttack == false)
        {
            
            if (pi.mouse0A)
            {
                anim.SetBool("R0L1", false);
                anim.SetTrigger("attack");
                canRunAttack = false;
            }
            else if (pi.counterBack && leftIsShu)
            {
                anim.SetBool("R0L1", true);
                anim.SetTrigger("counterBack");
                canRunAttack = false;
             
            }
        }


        if (leftIsShu)
        {
            if ( (CheckState("ground") || CheckState("blocked")) && pi.kunai)
            {
                anim.SetBool("kunai",true);
                anim.SetLayerWeight(anim.GetLayerIndex("kunai"), 1);
                DWup();
            }
            else
            {
                anim.SetBool("kunai", false);
                anim.SetLayerWeight(anim.GetLayerIndex("kunai"), 0);
                DWdown();
            }
        }
        else
        {
            anim.SetLayerWeight(anim.GetLayerIndex("kunai"), 0f);
        }

     

        if (cameraController.lockState == false)
        {
            if (pi.Dmag > 0.1f)
            {
                model.transform.forward = Vector3.Slerp(model.transform.forward, pi.Dvec, 0.2f);
            }
            if (lockMove == false)
            {
                movingVec = pi.Dmag * model.transform.forward * walkSpeed * ((pi.run) ? runMultiplier : 1.0f);
            }
        }
        else
        {
            if (trackDirection == false)
            {
                model.transform.forward = transform.forward;
            }
            else
            {
                model.transform.forward = movingVec.normalized;
            }

            if (lockMove == false)
            {
                movingVec = pi.Dvec * walkSpeed * ((pi.run) ? runMultiplier : 1.0f);
            }

        }

    }

    private void FixedUpdate()
    {
        rigid.position += deltaPosition;

        rigid.velocity = new Vector3(movingVec.x, rigid.velocity.y, movingVec.z) + thrustVec;
        thrustVec = Vector3.zero;

        deltaPosition = Vector3.zero;
    }

     public bool CheckState (string statename, string layerName = "Base Layer")
    {
        return anim.GetCurrentAnimatorStateInfo(anim.GetLayerIndex(layerName)).IsName(statename);
    }
    public bool CheckTag(string tagName, string layerName = "Base Layer")
    {
        return anim.GetCurrentAnimatorStateInfo(anim.GetLayerIndex(layerName)).IsTag(tagName);
    }

    void DWup()
    {
        defenceWall.SetActive(true);
    }
    void DWdown()
    {
        defenceWall.SetActive(false);
    }

    /// 
    /// 
    /// 

    public void OnJumpEnter()
    {
        thrustVec = new Vector3(0, jumpVelocity, 0);
        pi.inputEnabled = false;
        lockMove = true;
        trackDirection = true;
    }

    public void IsGround()
    {
        anim.SetBool("ground", true);
    }

    public void IsNotGround()
    {
        anim.SetBool("ground", false);
    }

    public void OnGroundEnter()
    {
        pi.inputEnabled = true;
        lockMove = false;
        canAttack = true;
        col.material = friction1;
        trackDirection = false;
    }

     public void OnGroundExit()
    {
        col.material = friction0;
    }

    public void OnFallEnter()
    {
        pi.inputEnabled = false;
        lockMove = true;
    }

    public void OnRollEnter()
    {
        thrustVec = new Vector3(0, rollVelocity, 0);
        pi.inputEnabled = false;
        lockMove = true;
        trackDirection = true;
    }

    public void OnJabEnter()
    {
        pi.inputEnabled = false;
        lockMove = true;
    }

    public void OnJabUpdate()
    {
        thrustVec = model.transform.forward * anim.GetFloat("jabVelocity");
       
    }

    public void OnAttack1AEnter()
    {
        pi.inputEnabled = false;
    }

    public void OnAttack1AUpdate()
    {
        thrustVec = model.transform.forward * anim.GetFloat("attack1AVelocity");
        
    }

    public void OnAttackExit()
    {
        model.SendMessage("WeaponDisable");
    }
    public void OnHitEnter()
    {
        pi.inputEnabled = false;
        movingVec = Vector3.zero;
        model.SendMessage("WeaponDisable");
    }

    public void OnDieEnter()
    {
        pi.inputEnabled = false;
        movingVec = Vector3.zero;
        model.SendMessage("WeaponDisable");
    }

    public void OnBlockedEnter()
    {
        pi.inputEnabled = false;
    }

    public void OnStunnedEnter()
    {
        pi.inputEnabled = false;
        movingVec = Vector3.zero;
    }

    public void OnCounterBackEnter()
    {
        pi.inputEnabled = false;
        movingVec = Vector3.zero;
    }


    public void OnUpdateRM(object _deltaPosition)
    {
        if (CheckState("attack1C") || CheckState("attack1C0") )
        {
            deltaPosition += (deltaPosition + (Vector3)_deltaPosition) / 2.0f;
        }
    }

    public void IssueTrigger (string triggerName)
    {
        anim.SetTrigger(triggerName);
    }

}
