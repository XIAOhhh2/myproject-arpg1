using System.Collections;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using UnityEngine;

public class PlayerButton
{
    public bool IsPressing = false;
    public bool OnPressed  = false;
    public bool OnReleased = false;
    public bool IsExtending = false;
    public bool IsDelaying = false;

    public float extendingDuration = 0.15f;
    public float delayingDuration = 0.15f;

    private bool curState = false;
    private bool lastState = false;

    private InputTimer exTimer = new InputTimer();
    private InputTimer deTimer = new InputTimer();


    public void Tick(bool input)
    {

        exTimer.Tick();
        deTimer.Tick();

        curState = input;

        IsPressing = curState;

        OnPressed = false;
        OnReleased = false;
        IsExtending = false;
        IsDelaying = false;

        if (curState != lastState)
        {
            if (curState == true)
            {
                OnPressed = true;
                StartTimer(deTimer, delayingDuration);
            }
            else
            {
                OnReleased = true;
                StartTimer(exTimer, extendingDuration);
                                     
            }
        }

        lastState = curState;

        if (exTimer.state == InputTimer.STATE.RUN)
        {
            IsExtending = true;
        }
    
        if (deTimer.state == InputTimer.STATE.RUN)
        {
            IsDelaying = true;
        }
    } 
    
    private void StartTimer(InputTimer timer ,float duration) 
    {
        timer.duration = duration;
        timer.Do();

    }

}
