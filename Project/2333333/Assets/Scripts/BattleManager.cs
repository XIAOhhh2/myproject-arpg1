using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CapsuleCollider))]
public class BattleManager : IActorManagerInterface
{
    private CapsuleCollider defenceCollider;
    public WeaponController targetWc;

     void Start()
    {
        defenceCollider = GetComponent<CapsuleCollider>();
        defenceCollider.center = Vector3.up * 1.0f;
        defenceCollider.radius = 0.4f;
        defenceCollider.height = 2f;
        defenceCollider.isTrigger = true;
        
    }
    public void OnTriggerEnter(Collider other)
    {
        targetWc = other.GetComponentInParent<WeaponController>();

        GameObject attacker = (targetWc.tag == "Boss")? targetWc.bc.gameObject: targetWc.ec.gameObject;
        GameObject receiver = am.gameObject;

        Vector3 attackingDir = receiver.transform.position - attacker.transform.position;
        Vector3 counterDir = attacker.transform.position - receiver.transform.position;

        float attackingAngle1 = Vector3.Angle(attacker.transform.forward, attackingDir);
        float counterAngle1 = Vector3.Angle(receiver.transform.forward, counterDir);
        float counterAngle2 = Vector3.Angle(attacker.transform.forward, receiver.transform.forward);
        
        bool attcckValid = (attackingAngle1 < 75);
        bool counterValid = (counterAngle1 < 60 && Mathf.Abs(counterAngle2 - 180) < 45);

        if (other.tag == "Weapon")
        {
                am.TryDoDamage(targetWc,attcckValid, counterValid);  
        }   
    }
    
}
