using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WweaponManager : IActorManagerInterface
{
    public Collider weaponColliderL;
    public Collider weaponColliderR;

    public GameObject whL;
    public GameObject whR;

    public WeaponController wcL;
    public WeaponController wcR;

    void Start()
    {
        whL = transform.DeepFind("WeaponHandleL").gameObject;
        whR = transform.DeepFind("WeaponHandleR").gameObject;

        wcL = BindWeaponController(whL);
        wcR = BindWeaponController(whR);

        weaponColliderL = whL.GetComponentInChildren<Collider>();
        weaponColliderR = whR.GetComponentInChildren<Collider>();

        weaponColliderL.enabled = false;
        weaponColliderR.enabled = false;

    }

    public WeaponController BindWeaponController(GameObject targetObject)
    {
        WeaponController tempWc;
        tempWc = targetObject.GetComponent<WeaponController>();
        if (tempWc == null)
        {
            tempWc = targetObject.AddComponent<WeaponController>();
        }
        tempWc.wm = this;

        return tempWc;
    }

    public void WeaponEnable()
    {
        if (am.ac.CheckTag("attackL"))
        {
            weaponColliderL.enabled = true;
        }
        else 
        {
            weaponColliderR.enabled = true;
        }

    }
    public void WeaponDisable()
    {
      
            weaponColliderL.enabled = false;
            weaponColliderR.enabled = false;
    
    }

    public void CounterBackEnable()
    {
        am.SetIsCounterBack(true);
    }

    public void CounterBackDisable()
    {
        am.SetIsCounterBack(false);
    }

}
