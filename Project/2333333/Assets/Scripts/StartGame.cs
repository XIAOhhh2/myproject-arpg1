using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartGame : MonoBehaviour
{
    public Text text;
    public Button button;
    public Button back;
    public string _string;

    
    public void LoadGame()
    {
        SceneManager.LoadScene("City");
        
    }

    public void Mission()
    {
     
            text.text = _string;
            button.gameObject.SetActive(true);
            back.gameObject.SetActive(true);

    }
    public void StartGameB()
    {
        SceneManager.LoadScene("BattleScene");

    }
    public void StartLearn()
    {
        SceneManager.LoadScene("SampleScene");
    }
    public void ExitGame()
    {
        //UnityEditor.EditorApplication.isPlaying = false;
        Application.Quit();

    }
} 
