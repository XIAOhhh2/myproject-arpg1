using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MenuManager : MonoBehaviour
{

    public Button back;
    public Button exit;
    public bool menu;
    public GameObject p;
    public KeyInPut kp;
    public GameObject ms;


    public void Awake()
    {
        kp = p.GetComponent<KeyInPut>();
    }
    private void Start()
    {
        back.gameObject.SetActive(false);
        exit.gameObject.SetActive(false);
        ms.SetActive(false);
        menu = false;
  
    }
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            if (menu == false)
            {
                back.gameObject.SetActive(true);
                exit.gameObject.SetActive(true);
                ms.SetActive(true);
                menu = true;
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.Confined;
                kp.mouseEnable = false;

            }

            else
            {
                back.gameObject.SetActive(false);
                exit.gameObject.SetActive(false);
                ms.SetActive(false);
                menu = false;
                Cursor.visible = false;
                kp.mouseEnable = true;

            }
        }
    }
}
   
