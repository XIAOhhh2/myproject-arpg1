using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public GameObject player;
    private StateManager sm;
    private ActorManager am;
    public GameObject ps;

    private void Awake()
    {
        sm = player.GetComponent<StateManager>();
        am = player.GetComponent<ActorManager>();
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if( this.tag == "AddMaxHealth")
            {
                sm.HPMax += 100;
                Instantiate(ps, this.transform.position + new Vector3(2, 2, 2), this.transform.rotation);
                this.gameObject.SetActive(false);
            }
            else if(this.tag == "AddDamage")
            {
                am.attackDamage += 2;
                Instantiate(ps, this.transform.position + new Vector3(2,2,2), this.transform.rotation);
                this.gameObject.SetActive(false);
            }
        }
    }
}
