using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;


//[RequireComponent(typeof(RotationMap))]
public class BossController : MonoBehaviour
{
    public enum BossEnemyState
    {
        Idle, TurnAround, Walk, Run, Attack, Defence, CounterBack, Stunned, Hit, Dead 
    }


    public delegate void BossThinkMethod();
    public Animator animator;

    [Header("speed")]
    public float rotateSpeed;

    [Header("time")]
    public float thinkTimeOfIdle;
    public float thinkTimeOfAttack;
    //public float animationDampTime;
    //public float destroyDelayTime;
    //public float attackCoolTime;

    [Header("Distance")]
    public float inspectDistance;
    public float standAttackDistance;

    [Header("ss")]
    public BossEnemyState bossEnemyState;
    public string playerTag;
    public float maxHealth;
    public float causeDamage;
    public bool isDie;


   [SerializeField]
    private float health;
    private bool resetMap;
    private GameObject player;
    private Quaternion targetRotation;

    private const int RotateAngle = 45;
    [SerializeField]
    private Transform[] enemyTransform;
    private int transformPoint;
    private NavMeshAgent navMeshAgent;
    private Camera m_camera;
    private float j;
    private float t;
    private bool cBack = false;
    private bool action;
    private bool learn = false;

    public Slider slider;
    public CapsuleCollider co;
    //public GameObject defenceWall;

    private StateManager sm;
    private ActorManager am;
    

    public GameObject whL;
    public GameObject whR;

    public WeaponController wcL;
    public WeaponController wcR;
    public Collider weaponColliderL;
    public Collider weaponColliderR;
    public bool nav;


    private void Awake()
    {
        animator = GetComponent<Animator>();
        navMeshAgent = GetComponent<NavMeshAgent>();
        co = GetComponent<CapsuleCollider>();
        m_camera = Camera.main;
 
    }

    void Start()
    {
        health = maxHealth;
        player = GameObject.FindGameObjectWithTag("Player");
        sm = player.GetComponent<StateManager>();
        am = player.GetComponent<ActorManager>();
        //navMeshAgent.autoBraking = false;


        whL = transform.DeepFind("WeaponHandleL").gameObject;
        whR = transform.DeepFind("WeaponHandleR").gameObject;

        wcL = BindWeaponController(whL);
        wcR = BindWeaponController(whR);


        weaponColliderL = whL.GetComponentInChildren<Collider>();
        weaponColliderR = whR.GetComponentInChildren<Collider>();

        weaponColliderL.enabled = false;
        weaponColliderR.enabled = false;

        //defenceWall.SetActive(false);
        if (slider == null)
        {
            GameObject sl = transform.DeepFind("BossHP").gameObject;
            slider = sl.GetComponent<Slider>();
            learn = true;
        }
        else slider.gameObject.SetActive(false);

       
    }

    public WeaponController BindWeaponController(GameObject targetObject)
    {
        WeaponController tempWc;
        tempWc = targetObject.GetComponent<WeaponController>();
        if (tempWc == null)
        {
            tempWc = targetObject.AddComponent<WeaponController>();
        }
        tempWc.bc = this;

        return tempWc;
    }
    



    void Update()
    {

        //Random.InitState(1);
        j = Random.Range(0, 4);
    
        if (isDie == false)
        {
            ControlState();
            ControlRotation();
        }
        ControlAnimation();

        if (!navMeshAgent.pathPending && navMeshAgent.remainingDistance < 0.5f)
        {
            nav = true;
        }

    }

    void ControlSlider()
    {
        //Debug.DrawRay(transform.position, transform.up * 5, Color.red, 500);

        slider.value = (float)health / maxHealth;
    }

    void ControlState()
    {
        float distance = Vector3.Distance(transform.position, player.transform.position);
        if (distance > inspectDistance)
        {
            
            InvokeThinkMethod(RunState, ThinkIdleState, thinkTimeOfIdle);
            
            nav = false;
        }
        else
        {
            
            navMeshAgent.destination = player.transform.position - new Vector3(1f, 0f, 1f);
            if (distance > standAttackDistance)
            {
                slider.gameObject.SetActive(true);
                ControlSlider();
                InvokeThinkMethod(ThinkIdleState, RunState, thinkTimeOfAttack);

            }
            else
            {
                ControlSlider();
                if (t >= thinkTimeOfAttack)
                {
                    Defence();
                    t = 0;
                }
                t += Time.deltaTime;
            }

        }
    }

    void InvokeThinkMethod(BossThinkMethod cancelMethod, BossThinkMethod invokeMethod, float delay)
    {
        CancelInvoke(GetMethodName(cancelMethod));
        if (!IsInvoking(GetMethodName(invokeMethod)))
            Invoke(GetMethodName(invokeMethod), delay);
    }

    string GetMethodName(BossThinkMethod th) { return th.Method.Name; }

    void ThinkIdleState()
    {
        bossEnemyState = BossEnemyState.Idle;
        targetRotation = (bossEnemyState == BossEnemyState.TurnAround) ? Quaternion.Euler(0, 90, 0) : targetRotation;
        if (bossEnemyState == BossEnemyState.TurnAround)
            bossEnemyState = BossEnemyState.Walk;
    }
    void RunState()
    {
        bossEnemyState = BossEnemyState.Run;

    }
    void ThinkAttackState()
    {
        switch (j)
        {
            case 0:
                bossEnemyState = BossEnemyState.Attack;
                break;
            case 1:
                bossEnemyState = BossEnemyState.Attack;
                break;
            case 2:
                bossEnemyState = BossEnemyState.Defence;
                break;
            case 3:
                bossEnemyState = BossEnemyState.CounterBack;
                break;
  
        }

    }
    void ControlAnimation()
    {

        switch (bossEnemyState)
        {
            case BossEnemyState.Idle:
                animator.SetFloat("walk", 0);
                weaponColliderL.enabled = false;
                animator.SetBool("kunai", false);
                animator.SetBool("counterBack", false);
                break;
            case BossEnemyState.TurnAround:
            case BossEnemyState.Walk:
                animator.SetFloat("walk", 1);
                weaponColliderL.enabled = false;
                animator.SetBool("kunai", false);
                break;
            case BossEnemyState.Run:
                animator.SetFloat("walk", 2);
                weaponColliderL.enabled = false;
                animator.SetBool("kunai", false);
                animator.SetBool("counterBack", false);
                break;
            case BossEnemyState.Attack:
                animator.SetTrigger("attack");
                animator.SetFloat("walk", 0);
                weaponColliderL.enabled = false;
                animator.SetBool("kunai", false);
                animator.SetBool("counterBack", false);
                break;
            case BossEnemyState.Defence:
                // defenceWall.SetActive(true);
                weaponColliderL.enabled = true;
                animator.SetBool("kunai", true);
                animator.SetBool("counterBack", false);
                break;
            case BossEnemyState.CounterBack:
                animator.SetBool("counterBack", true);
                break;
            case BossEnemyState.Stunned:
            case BossEnemyState.Dead:
                weaponColliderL.enabled = false;
                break;
        }

    }


    void ControlRotation()
    {
        Vector3 center = new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z);

        switch (bossEnemyState)
        {
            case BossEnemyState.TurnAround:
                transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * rotateSpeed);
                break;
            case BossEnemyState.Run:

            case BossEnemyState.Attack:
                transform.LookAt(center);
                break;
            case BossEnemyState.Hit:
                transform.LookAt(center);
                break;
            case BossEnemyState.Defence:
                transform.LookAt(center);
                break;


        }
    }
    public bool CheckState(string statename, string layerName = "Base Layer")
    {
        return animator.GetCurrentAnimatorStateInfo(animator.GetLayerIndex(layerName)).IsName(statename);
    }


    public void WeaponEnable()
    {

        //weaponColliderL.enabled = true;
        weaponColliderR.enabled = true;

    }
    public void WeaponDisable()
    {

        //weaponColliderL.enabled = false;
        weaponColliderR.enabled = false;

    }


    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PlayerWeapon")
        {
            TryDamage();
        }
    }

    private void Defence()
    {
        /*int[] dcj = new int[5];
        dcj[0] = (int)BossEnemyState.Attack;
        dcj[1] = (int)BossEnemyState.CounterBack;
        dcj[2] = (int)BossEnemyState.Attack;
        dcj[3] = (int)BossEnemyState.Attack;
        dcj[4] = (int)BossEnemyState.Defence;
        */
        
            if (action && bossEnemyState != BossEnemyState.Stunned )
            {
           
                ThinkAttackState();
               /* bossEnemyState = (BossEnemyState) dcj[j];
                print(j);
                if (j == dcj[0])
                {
                    float i = 0f;
                    i += Time.deltaTime;
                    if (i > 1.5)
                    {
                        ControlState();
                    }
                }
            }
            else if(bossEnemyState != BossEnemyState.Stunned)
            {
                j = 1;
                InvokeThinkMethod(ThinkIdleState, ThinkAttackState, thinkTimeOfAttack);
               */
            }
    }

    public void TryDamage()
    {
        if (cBack)
        {
            am.Stunned();
        }

        else if(CheckState("kunai"))
        {
            animator.SetTrigger("blocked");

        }
        else
            GetDamage(sm.am.attackDamage);
    }

    public void GetDamage(float damage)
    {
        if (sm.isCounterBackSuccess)
        {

            Stunned();

        }

        animator.SetTrigger("hit");
        weaponColliderL.enabled = false;
        health -= damage;

        if (health <= 0)
        {
            isDie = true;
            bossEnemyState = BossEnemyState.Dead;
            animator.SetTrigger("die");
            //animator.SetFloat("walk", 0);
            WeaponDisable(); Destroy(co);
            action = false;
            slider.gameObject.SetActive(false);
            //transform.position = new Vector3(transform.position.x, -1, transform.position.z);
            if (learn) Destroy(this.gameObject,1.5f);
        }
    }

    public void Stunned()
    {
        
        bossEnemyState = BossEnemyState.Stunned;
        animator.SetTrigger("stunned");
        weaponColliderL.enabled = false;

    }
    public void OnStunnedExit()
    {
        bossEnemyState = BossEnemyState.Idle;

    }

    public void CounterBackEnable()
    {
       cBack = true;
    }

    public void CounterBackDisable()
    {
        cBack = false;
    }
    public void OnAttackExit() {WeaponDisable();}
    public void OnGroundEnter() { action = true; }
    public void OnCounnterBackEnter() { action = false; }
    public void OnHitEnter() { action = false; WeaponDisable(); }
    public void OnStunnedEnter() { action = false; }
    public void OnAttackEnter() { action = false; }
    public void OnBlockedEnter() { action = false; }

}

