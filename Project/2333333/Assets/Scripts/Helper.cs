using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Helper", menuName = "CreateHelper")]
public class Helper : ScriptableObject
{
    [SerializeField]
    private Sprite icon;

    [SerializeField]
    private string KeyName;

    [SerializeField]
    private string information;


    public Sprite GetIcon()
    {
        return icon;
    }

    public string GetKeyName()
    {
        return KeyName;
    }

    public string GetInformation()
    {
        return information;
    }

}