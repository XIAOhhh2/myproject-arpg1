using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class M : MonoBehaviour
{
    public ScrollRect srI;
    
    [SerializeField]
    private Image image1;
    [SerializeField]
    private Image image2;
    [SerializeField]
    private Image image3;
    [SerializeField]
    private Image image4;
    [SerializeField]
    private Image image5;
    [SerializeField]
    private Image image6;
    [SerializeField]
    private Image image7;
    [SerializeField]
    private Image image8;
    //public Image[] images;
    public ScrollRect srT;
    [SerializeField]
    private Text text1;
    [SerializeField]
    private Text text2;
    [SerializeField]
    private Text text3;
    [SerializeField]
    private Text text4;
    [SerializeField]
    private Text text5;
    [SerializeField]
    private Text text6;
    [SerializeField]
    private Text text7;
    [SerializeField]
    private Text text8;

    private bool helper;

    [SerializeField]
    private HelperDataBase helperDataBase;

    private Dictionary<Helper, int> numOfHelper = new Dictionary<Helper, int>();

    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < helperDataBase.GetHelperLists().Count; i++)
        {
            numOfHelper.Add(helperDataBase.GetHelperLists()[i], i);        
        }
        image1.sprite = helperDataBase.GetHelperLists()[0].GetIcon();
        image2.sprite = helperDataBase.GetHelperLists()[1].GetIcon();
        image3.sprite = helperDataBase.GetHelperLists()[2].GetIcon();
        image4.sprite = helperDataBase.GetHelperLists()[3].GetIcon();
        image5.sprite = helperDataBase.GetHelperLists()[4].GetIcon();
        image6.sprite = helperDataBase.GetHelperLists()[5].GetIcon();
        image7.sprite = helperDataBase.GetHelperLists()[6].GetIcon();
        image8.sprite = helperDataBase.GetHelperLists()[7].GetIcon();
        text1.text = helperDataBase.GetHelperLists()[0].GetInformation();
        text2.text = helperDataBase.GetHelperLists()[1].GetInformation();
        text3.text = helperDataBase.GetHelperLists()[2].GetInformation();
        text4.text = helperDataBase.GetHelperLists()[3].GetInformation();
        text5.text = helperDataBase.GetHelperLists()[4].GetInformation();
        text6.text = helperDataBase.GetHelperLists()[5].GetInformation();
        text7.text = helperDataBase.GetHelperLists()[6].GetInformation();
        text8.text = helperDataBase.GetHelperLists()[7].GetInformation();
        srI.gameObject.SetActive(false);
        srT.gameObject.SetActive(false);

        helper = false;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.H))
        {
            if (helper == false)
            {
                srI.gameObject.SetActive(true);
                srT.gameObject.SetActive(true);
                helper = true;
            }

            else
            {
                srI.gameObject.SetActive(false);
                srT.gameObject.SetActive(false);
                helper = false;

            }
        }
    }


    public Helper GetHelper(string name)
    {
        return helperDataBase.GetHelperLists().Find(helperName => helperName.GetKeyName() == name);

    }


}