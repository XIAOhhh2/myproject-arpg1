using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIBattlemanager : MonoBehaviour
{
    public EnemyController ec;
    public CapsuleCollider co;
    private void Awake()
    {
        ec = GetComponent<EnemyController>();
    }
    // Start is called before the first frame update
    void Start()
    {
        co = GetComponent<CapsuleCollider>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PlayerWeapon")
        {
            print("222");
            ec.GetDamage(2);
        }
    }

}
