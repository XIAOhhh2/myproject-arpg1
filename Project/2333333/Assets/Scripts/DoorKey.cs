using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorKey : MonoBehaviour
{
    public GameObject enemy;
    private EnemyController ec;

    private void Awake()
    {
        ec = enemy.GetComponent<EnemyController>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (ec.isDie)
        {
            this.gameObject.SetActive(false);
        }
    }
}
