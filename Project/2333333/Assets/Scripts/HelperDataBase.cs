using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "HelperDataBase", menuName = "CreateHelperDataBase")]
public class HelperDataBase : ScriptableObject
{

    [SerializeField]
    private List<Helper> helperLists = new List<Helper>();

    public List<Helper> GetHelperLists()
    {
        return helperLists;
    }
}