using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ChangeValue : MonoBehaviour
{
    public Slider maxHealth;
    public Slider damage;
    public GameObject player;
    private StateManager sm;
    private ActorManager am;

    private void Awake()
    {
        sm = player.GetComponent<StateManager>();
        am = player.GetComponent<ActorManager>();
    }


    public void Update()
    {
        sm.HPMax = maxHealth.value;
        am.attackDamage = damage.value;

    }


}
