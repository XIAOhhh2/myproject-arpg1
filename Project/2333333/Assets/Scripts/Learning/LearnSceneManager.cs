using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LearnSceneManager : MonoBehaviour
{
    private bool menu;
    public Button back;
    public Button exit;
    public Button reset;
    public ScrollRect sr;
    public GameObject player;
    public Image image;
    public Slider maxHealth;
    public Slider damage;
    public Text mhT;
    public Text daT;
    public GameObject ms;
    private KeyInPut kp;
    private StateManager sm;

    private void Awake()
    {
        kp = player.GetComponent<KeyInPut>();
        sm = player.GetComponent<StateManager>();
    }

    // Start is called before the first frame update
    void Start()
    {
        back.gameObject.SetActive(false);
        exit.gameObject.SetActive(false);
        reset.gameObject.SetActive(false);
        sr.gameObject.SetActive(false);
        image.gameObject.SetActive(false);
        maxHealth.gameObject.SetActive(false);
        damage.gameObject.SetActive(false);
        mhT.gameObject.SetActive(false);
        daT.gameObject.SetActive(false);
        ms.SetActive(false);
        menu = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            if (menu == false)
            {
                back.gameObject.SetActive(true);
                exit.gameObject.SetActive(true);
                reset.gameObject.SetActive(true);
                sr.gameObject.SetActive(true);
                maxHealth.gameObject.SetActive(true);
                damage.gameObject.SetActive(true);
                mhT.gameObject.SetActive(true);
                daT.gameObject.SetActive(true);
                ms.SetActive(true);

                menu = true;
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.Confined;
                kp.mouseEnable = false;
                kp.inputEnabled = false;

            }

            else
            {
                back.gameObject.SetActive(false);
                exit.gameObject.SetActive(false);
                reset.gameObject.SetActive(false);
                maxHealth.gameObject.SetActive(false);
                damage.gameObject.SetActive(false);
                mhT.gameObject.SetActive(false);
                daT.gameObject.SetActive(false);
                sr.gameObject.SetActive(false);
                ms.SetActive(false);
                menu = false;
                Cursor.visible = false;
                kp.mouseEnable = true;
                kp.inputEnabled = true;

            }
        }
        if (sm.HP <= 0) PlayerDie();

    }
    public void PlayerDie()
    {

        image.gameObject.SetActive(true);
        exit.gameObject.SetActive(true);
        reset.gameObject.SetActive(true);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
        kp.mouseEnable = false;
        kp.inputEnabled = false;

    }

    public void ResetScene()
    {
        SceneManager.LoadScene("SampleScene");
    }

    public void MenuBack()
    {
        back.gameObject.SetActive(false);
        exit.gameObject.SetActive(false);
        reset.gameObject.SetActive(false);
        sr.gameObject.SetActive(false);
        ms.SetActive(false);
        mhT.gameObject.SetActive(false);
        daT.gameObject.SetActive(false);
        maxHealth.gameObject.SetActive(false);
        damage.gameObject.SetActive(false);
        menu = false;
        Cursor.visible = false;
        kp.mouseEnable = true;
        kp.inputEnabled = true;
    }

    public void QuitGame()
    {
        SceneManager.LoadScene("StartGame");
    }
}
