using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyClone : MonoBehaviour
{
    public GameObject p;
    private Vector3 vector3;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    [System.Obsolete]
    void Update()
    {
        vector3 = new Vector3(Random.RandomRange(-22, 22), 1, Random.RandomRange(-22, 22));
    }
    public void EnemyCloneM()
    {
        Instantiate(p, vector3, Quaternion.identity);
    }
}
