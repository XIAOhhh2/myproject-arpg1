using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddHealth : MonoBehaviour
{
    public GameObject enemy;
    public Transform transform1;
    public Transform transform2;



    private void OnTriggerEnter(Collider other)
    {
   
        if(other.tag == "Player")
        {
            if (this.tag == "ningu")
            {
                StateManager sm = other.GetComponent<StateManager>();
                sm.AddHP(200.0f);
            }
            else
            {
                StateManager sm = other.GetComponent<StateManager>();
                sm.AddHP(200.0f);
                Instantiate(enemy, transform1.position, Quaternion.identity);
                Instantiate(enemy, transform2.position, Quaternion.identity);

            }

        }
    }
}
