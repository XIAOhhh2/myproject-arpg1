using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContinueGame : MonoBehaviour
{
    public Button back;
    public Button exit;
    public Button mission;
    public GameObject p;
    private KeyInPut kp;
    public GameObject ms;
    private void Awake()
    {
        kp = p.GetComponent<KeyInPut>();
    }

    public void Continue()
    {
        back.gameObject.SetActive(false);
        exit.gameObject.SetActive(false);
        mission.gameObject.SetActive(false);
        ms.SetActive(false);
        Cursor.visible = false;
        kp.mouseEnable = true;

    }
}
