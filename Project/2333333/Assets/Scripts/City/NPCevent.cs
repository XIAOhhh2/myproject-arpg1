using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NPCevent : MonoBehaviour
{
    public Text text;
    public Image image;
    public string _string;

    // Start is called before the first frame update
    void Start()
    {
        text.gameObject.SetActive(false);
        image.gameObject.SetActive(false);
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter(Collider other)
    {
       if ( other.tag == "Player")
        {
            image.gameObject.SetActive(true);
            text.gameObject.SetActive(true);
            text.text = _string;
        }

    }
    public void OnTriggerExit (Collider other)
    {
        if (other.tag == "Player")
        {
            image.gameObject.SetActive(false);
            text.gameObject.SetActive(false);
        }

    }

}
