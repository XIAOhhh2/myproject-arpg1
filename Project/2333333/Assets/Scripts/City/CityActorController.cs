﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityActorController : MonoBehaviour
{
    public GameObject model;
  
    public IUserInput pi;
    public float walkSpeed = 1.4f;
    public float runMultiplier = 2.0f;
    public float jumpVelocity = 5.0f;

    public PhysicMaterial friction1;
    public PhysicMaterial friction0;

    public Animator anim;
    public Rigidbody rigid;
    private Vector3 movingVec;
    private Vector3 thrustVec;

    private bool lockMove = false;
   
    private CapsuleCollider col;
    private Vector3 deltaPosition;


    void Awake()
    {
        IUserInput[] inputs = GetComponents<IUserInput>();
        foreach (var input in inputs)
        {
            if (input.enabled == true)
            {
                pi = input;
                break;
            }
        }
        anim = model.GetComponent<Animator>();
        rigid = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();
    }

    // Update is called once per frame
    void Update()
    {
        if (pi.run)
        {
            anim.SetFloat("walk", pi.Dmag * Mathf.Lerp(anim.GetFloat("walk"), 2.0f, 0.02f));
        }
        else
        {
            anim.SetFloat("walk", pi.Dmag * Mathf.Lerp(anim.GetFloat("walk"), 1.0f, 0.4f));
        }

        if (pi.Dmag > 0.1f)
        {
            model.transform.forward = Vector3.Slerp(model.transform.forward, pi.Dvec, 0.2f);
        }
        if (lockMove == false)
        {
            movingVec = pi.Dmag * model.transform.forward * walkSpeed * ((pi.run) ? runMultiplier : 1.0f);
        }



        if ((pi.mouse0 || pi.mouse1) && CheckState("ground") )
        {
           
        }


 
    }

    private void FixedUpdate()
    {
        rigid.position += deltaPosition;

        rigid.velocity = new Vector3(movingVec.x, rigid.velocity.y, movingVec.z)+ thrustVec;
        thrustVec = Vector3.zero;

        deltaPosition = Vector3.zero;
    }

    public bool CheckState(string statename, string layerName = "Base Layer")
    {
        return anim.GetCurrentAnimatorStateInfo(anim.GetLayerIndex(layerName)).IsName(statename);
    }

    public void OnGroundEnter()
    {
        pi.inputEnabled = true;
        lockMove = false;
        col.material = friction1;
    }

    public void OnGroundExit()
    {
        col.material = friction0;
    }


}
