using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityCameraController : MonoBehaviour
{
    private IUserInput pi;
    public float horizontalSpeed = 100.0f;
    public float verticalSpeed = 80.0f;
    public float cameraDampVelue = 0.8f;

    private GameObject playerHandle;
    private GameObject cameraHandle;
    private float EnlerX;
    private GameObject model;
    public GameObject newCamera;

    private Vector3 cameraVelocity;


    // Start is called before the first frame update
    void Start()
    {
        cameraHandle = transform.parent.gameObject;
        playerHandle = cameraHandle.transform.parent.gameObject;
        EnlerX = 20;
        CityActorController ac = playerHandle.GetComponent<CityActorController>();
        model = ac.model;
        pi = ac.pi;

        Cursor.lockState = CursorLockMode.Locked;
        //newCamera = Camera.main.gameObject;


    }

    // Update is called once per frame
    void Update()
    {

            Vector3 ModelEuler = model.transform.eulerAngles;

            playerHandle.transform.Rotate(Vector3.up, pi.Jright * horizontalSpeed * Time./*fixed*/deltaTime);
            EnlerX -= pi.Jup * verticalSpeed * Time.fixedDeltaTime;
            EnlerX = Mathf.Clamp(EnlerX, -30, 45);
            cameraHandle.transform.localEulerAngles = new Vector3(EnlerX, 0, 0);


    }
    private void FixedUpdate()
    {
   
        //newCamera.transform.position = Vector3.Lerp(newCamera.transform.position, transform.position, cameraDampVelue);
        newCamera.transform.position = Vector3.SmoothDamp(newCamera.transform.position, transform.position, ref cameraVelocity, cameraDampVelue);
        //newCamera.transform.eulerAngles = transform.eulerAngles;
        newCamera.transform.LookAt(cameraHandle.transform);

    }

}
