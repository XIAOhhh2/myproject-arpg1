using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCMove : MonoBehaviour
{
    [SerializeField] private Transform[]  npcTransform;
    private int transformpoint;
    
    private NavMeshAgent navMeshAgent;

   void  Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();

        navMeshAgent.autoBraking = false;
        Goto();
    }

     void Goto()
    {
        if (npcTransform.Length == 0)
        {
            return;
        }
        navMeshAgent.destination = npcTransform[transformpoint].position;

        transformpoint = (transformpoint + 1) % npcTransform.Length;
    }

    private void Update()
    {
        if (!navMeshAgent.pathPending && navMeshAgent.remainingDistance < 0.5f)
        {
            Goto();
        }
    }
}
