using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MissionEvent : MonoBehaviour
{
    public Text text;
    public Image image;
    public string _string;
    public GameObject p;
    public KeyInPut kp;
    public Button button;
    public Button back;
    public Button mission;


    public void Awake()
    {
        kp = p.GetComponent<KeyInPut>();
    }

    // Start is called before the first frame update
    void Start()
    {
        button.gameObject.SetActive(false);
        back.gameObject.SetActive(false);
        mission.gameObject.SetActive(false);
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        if ( other.tag == "Player")
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.Confined;
                kp.mouseEnable = false;

                image.gameObject.SetActive(true);
                text.gameObject.SetActive(true);
                text.text = _string;
                mission.gameObject.SetActive(true);
    

            }
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            image.gameObject.SetActive(false);
            text.gameObject.SetActive(false);
            mission.gameObject.SetActive(false);
            button.gameObject.SetActive(false);
            back.gameObject.SetActive(false);
            Cursor.visible = false;
            kp.mouseEnable = true;

        }

    }
}
