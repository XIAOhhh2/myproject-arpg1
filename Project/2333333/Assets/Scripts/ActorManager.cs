using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActorManager : MonoBehaviour
{
    public ActorController ac;
    public WweaponManager wm;
    public BattleManager bm;
    public StateManager sm;

    public float attackDamage;
    // Start is called before the first frame update
    public void Awake()
    {
        ac = GetComponent<ActorController>();

       GameObject sensor = transform.Find("Sensor").gameObject;
       GameObject model = ac.model;

        bm = Bind<BattleManager>(sensor);
        wm = Bind<WweaponManager>(model);
        sm = Bind<StateManager>(gameObject);
    }

     private T Bind <T>(GameObject go) where T : IActorManagerInterface
    {
        T tempInstance;
        tempInstance = go.GetComponent<T>();
        if (tempInstance == null)
        {
            tempInstance = go.AddComponent<T>();
        }
        tempInstance.am = this;
        return tempInstance;
    }


    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetIsCounterBack(bool value)
    {
        sm.isCounterBackEnable = value;
    }


    public void TryDoDamage(WeaponController targetWc, bool attackValid, bool counterValid)
    {
        if (sm.isCounterBackSuccess)
        {
            if (counterValid)
            {
                if (targetWc.tag == "Boss")
                {
                    targetWc.bc.Stunned();
                }   
                else   targetWc.ec.Stunned();
            }
        }
        else if (sm.isCounterBackFailure)
        {
            if (attackValid)
            {
                HitOrDie(false,0);
            }
        }
        else if(sm.isImmortal)
        {
        }
        else
        {
            if (sm.isKunai)
            {
                Blocked();
            }
            else
            {
                if (attackValid)
                {
                    HitOrDie(true,(targetWc.tag == "Boss")? targetWc.bc.causeDamage: targetWc.ec.causeDamage);
                }
            }
        }
    }

    public void Stunned()
    {
        ac.IssueTrigger("stunned");
    }

    public void Blocked()
    {
        ac.IssueTrigger("blocked");
    }

    public void HitOrDie(bool doHItAnim, float damege)
    {
        if (sm.HP <= 0)
        {

        }
        else
        {
            sm.AddHP( -damege);
            if (sm.HP > 0)
            {
                if (doHItAnim)
                {
                    Hit();
                }
            }
            else
            {
                Die();
            }
        }
    }

    public void Hit()
    {
        ac.IssueTrigger("hit");       
    }

    public void Die()
    {
        ac.IssueTrigger("die");
        ac.pi.inputEnabled = false;
       
        if (ac.cameraController.lockState == true)
        {
            ac.cameraController.LockUnlock();
        }
            ac.cameraController.enabled = false;
    }
}

