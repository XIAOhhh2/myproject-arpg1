using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class BattleSceneManager : MonoBehaviour
{
    public GameObject player;
    public GameObject boss;
    public Button back;
    public Button exit;
    public Button reset;
    public Image image;
    private KeyInPut kp;
    private bool menu;
    private StateManager sm;
    [SerializeField]
    private BossController bc;
    public Image image2;
    public Button button;
    public Text text;
    public Text text2;
    public Button b;
    public GameObject ms;
    private bool bossdie = true;


    public void Awake()
    {
        kp = player.GetComponent<KeyInPut>();
        sm = player.GetComponent<StateManager>();
        bc = boss.GetComponent<BossController>();
    }

    // Start is called before the first frame update
    public void Start()
    {
        back.gameObject.SetActive(false);
        exit.gameObject.SetActive(false);
        reset.gameObject.SetActive(false);
        image.gameObject.SetActive(false);
        menu = false;
        image2.gameObject.SetActive(false);
        button.gameObject.SetActive(false);
        text.gameObject.SetActive(false);
        text2.gameObject.SetActive(false);
        b.gameObject.SetActive(false);
        ms.SetActive(false);
    }

    // Update is called once per frame
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            if (menu == false)
            {
                back.gameObject.SetActive(true);
                exit.gameObject.SetActive(true);
                reset.gameObject.SetActive(true);
                ms.SetActive(true);
                menu = true;
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.Confined;
                kp.mouseEnable = false;

            }

            else
            {
                back.gameObject.SetActive(false);
                exit.gameObject.SetActive(false);
                reset.gameObject.SetActive(false);
                ms.SetActive(false);
                menu = false;
                Cursor.visible = false;
                kp.mouseEnable = true;

            }
        }
        if (sm.HP <= 0) PlayerDie();
        if (bc.isDie) BossDie();
    }

    public void PlayerDie()
    {

            image.gameObject.SetActive(true);
            exit.gameObject.SetActive(true);
            reset.gameObject.SetActive(true);
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.Confined;
            kp.mouseEnable = false;

    }

    public void BossDie()
    {
       if(bossdie)
        {
            image2.gameObject.SetActive(true);
            text.gameObject.SetActive(true);
            b.gameObject.SetActive(true);
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.Confined;
            kp.mouseEnable = false;
            bossdie = false;
        }

    }
    public void GameClear()
    {
        text.gameObject.SetActive(false);
        text2.gameObject.SetActive(true);
        exit.gameObject.SetActive(true);
        button.gameObject.SetActive(true);
        reset.gameObject.SetActive(true);

    }
    public void NextScene()
    {
        SceneManager.LoadScene("SampleScene");

    }
}
