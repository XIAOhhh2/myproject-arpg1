using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EventRotation : MonoBehaviour
{
    public GameObject player;
    public GameObject eventNPC;
    public Slider slider;


    private void Awake()
    {
        slider = GetComponent<Slider>();
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 vector3 = eventNPC.transform.position - player.transform.position;
        float angle = Vector3.SignedAngle(player.transform.forward, vector3 ,Vector3.up);
        slider.value = angle;
    }
}
