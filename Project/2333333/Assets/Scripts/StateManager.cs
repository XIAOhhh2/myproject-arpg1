using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StateManager : IActorManagerInterface
{

    public float HPMax = 20.0f;
    public float HP = 20.0f;
    public Slider hpSlider;


    public bool IsGround;
    public bool isJump;
    public bool isFall;
    public bool isRoll;
    public bool isJab;
    public bool isAttack;
    public bool isHit;
    public bool isDie;
    public bool isBlocked;
    public bool isKunai;
    public bool isCounterBack;
    public bool isCounterBackEnable;

    public bool isCanKuani;
    public bool isImmortal;
    public bool isCounterBackSuccess;
    public bool isCounterBackFailure;

     void Start()
    {
        HP = HPMax;
    }

     void Update()
    {
        hpSlider.value = HP/HPMax;

        IsGround = am.ac.CheckState("ground");
        isJump    = am.ac.CheckState("jump");
        isFall    = am.ac.CheckState("fall");
        isRoll    = am.ac.CheckState("roll");
        isJab     = am.ac.CheckState("jab");
        isAttack  = am.ac.CheckTag  ("attackR") || am.ac.CheckTag("attackL");
        isHit     = am.ac.CheckState("hit");
        isDie     = am.ac.CheckState("die");
        isBlocked = am.ac.CheckState("blocked");
        isCounterBack = am.ac.CheckState("counterBack");
        isCounterBackSuccess = isCounterBackEnable;
        isCounterBackFailure = isCounterBack && !isCounterBackEnable;

        isCanKuani = IsGround || isBlocked;
        isKunai   = isCanKuani && am.ac.CheckState("kunai","kunai");
        isImmortal = isRoll || isJab;
        
    }

    public void AddHP (float value)
    {
        HP += value;
        HP = Mathf.Clamp(HP, 0, HPMax);

    }



    public void Test()
    {
        print(HP);
    }
}
