﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using System.Collections;
using System.Collections.Generic;


public enum EnemyState
{
    Idle, TurnAround, Walk, Run, Attack, Dead
}


public delegate void ThinkMethod();

//[RequireComponent(typeof(RotationMap))]
public class EnemyController : MonoBehaviour
{
    public Animator animator;

    [Header("speed")]
    public float rotateSpeed;

    [Header("time")]
    public float thinkTimeOfIdle;
    public float thinkTimeOfAttack;
    public float animationDampTime;
    //public float destroyDelayTime;
    //public float attackCoolTime;

    [Header("Distance")]
    public float inspectDistance;
    public float standAttackDistance;

    [Header("ss")]
    public EnemyState enemyState;
    public string playerTag;
    public float maxHealth;
    public float causeDamage;
    public bool isDie;

    [SerializeField]
    private float health;
    private bool resetMap;
    private GameObject player;
    private Quaternion targetRotation;

    private const int RotateAngle = 45;
    [SerializeField]
    private Transform[] enemyTransform;
    private int transformPoint;
    private NavMeshAgent navMeshAgent;
    private Camera m_camera;
 

    public Slider slider;
    public CapsuleCollider co;

    public StateManager sm;

    public GameObject whL;
    public GameObject whR;

    public WeaponController wcL;
    public WeaponController wcR;
    public Collider weaponColliderL;
    public Collider weaponColliderR;
    public bool nav;


    private void Awake()
    {
       animator = GetComponent <Animator>();
        navMeshAgent = GetComponent<NavMeshAgent>();
        slider = GetComponentInChildren<Slider>();
        co = GetComponent<CapsuleCollider>();
        m_camera = Camera.main;
       
    }

    void Start()
    {
        health = maxHealth;
        player = GameObject.FindGameObjectWithTag("Player");
        sm = player.GetComponent<StateManager>();
        //navMeshAgent.autoBraking = false;
      
        //Goto();

        whL = transform.DeepFind("WeaponHandleL").gameObject;
        whR = transform.DeepFind("WeaponHandleR").gameObject;

        wcL = BindWeaponController(whL);
        wcR = BindWeaponController(whR);


        weaponColliderL = whL.GetComponentInChildren<Collider>();
        weaponColliderR = whR.GetComponentInChildren<Collider>();

        weaponColliderL.enabled = false;
        weaponColliderR.enabled = false;

        isDie = false;
    }

    public WeaponController BindWeaponController(GameObject targetObject)
    {
        WeaponController tempWc;
        tempWc = targetObject.GetComponent<WeaponController>();
        if (tempWc == null)
        {
            tempWc = targetObject.AddComponent<WeaponController>();
        }
        tempWc.ec = this;

        return tempWc;
    }


    IEnumerator Goto()
    {
        //enemyState = EnemyState.Idle;
        yield return new WaitForSeconds(2f);

        if (enemyTransform.Length == 0)
            {
                enemyState = EnemyState.Idle;
            }

       else if(nav == true && isDie == false)
        {
            navMeshAgent.destination = enemyTransform[transformPoint].position;

            transformPoint = (transformPoint + 1) % enemyTransform.Length;

        } 

        //yield return new WaitForSeconds(2f);
 
    }

    void Update()
    {
        ControlSlider();
        //ControlRotationMap();

        if (enemyState != EnemyState.Dead)
        {
            ControlState();
            //Goto();
            ControlRotation();
        }
        ControlAnimation();
        slider.transform.rotation = m_camera.transform.rotation;

        if (!navMeshAgent.pathPending && navMeshAgent.remainingDistance < 0.5f)
        {
            nav = true;
        }

    }


    void ControlSlider()
    {
        //Debug.DrawRay(transform.position, transform.up * 5, Color.red, 500);
       
        slider.value = (float)health / maxHealth;
    }

    void ControlState()
    {
        float distance = Vector3.Distance(transform.position, player.transform.position);
        if (distance > inspectDistance)
        {
            InvokeThinkMethod(ThinkAttackState, ThinkIdleState, thinkTimeOfIdle);
            StartCoroutine("Goto");
            nav = false;
        }
        else if (isDie == false)
        {
            StopCoroutine("Goto");
            navMeshAgent.destination = player.transform.position - new Vector3(1f,0f,1f);
            if (distance > standAttackDistance)
                InvokeThinkMethod(ThinkIdleState, ThinkAttackState, thinkTimeOfAttack);
            else
                enemyState = EnemyState.Attack;

        }
    }

    void InvokeThinkMethod(ThinkMethod cancelMethod, ThinkMethod invokeMethod, float delay)
    {
        CancelInvoke(GetMethodName(cancelMethod));
        if (!IsInvoking(GetMethodName(invokeMethod)))
            Invoke(GetMethodName(invokeMethod), delay);
    }

    string GetMethodName(ThinkMethod th) { return th.Method.Name; }

    void ThinkIdleState()
    {
        enemyState = /*(EnemyState)Random.Range((int)EnemyState.Idle, (int)*/EnemyState.Walk;
        targetRotation = (enemyState == EnemyState.TurnAround) ? Quaternion.Euler(0,90,0) : targetRotation;
        if (enemyState == EnemyState.TurnAround)
            enemyState = EnemyState.Walk;
    }

    void ThinkAttackState() { enemyState = EnemyState.Run; }


    void ControlAnimation()
    {

        switch (enemyState)
        {
            case EnemyState.Idle:
                animator.SetFloat("walk", 0, animationDampTime, Time.deltaTime);
                animator.SetBool("attack", false);
            
                break;
            case EnemyState.TurnAround:
            case EnemyState.Walk:
                animator.SetFloat("walk", 1, animationDampTime, Time.deltaTime);
                animator.SetBool("attack", false);
             
                break;
            case EnemyState.Run:
                animator.SetFloat("walk", 2, animationDampTime, Time.deltaTime);
                animator.SetBool("attack", false);
                
                break;
            case EnemyState.Attack:
                animator.SetBool("attack", true);
                animator.SetFloat("walk", 0);
 
                break;
            case EnemyState.Dead:
   

                break;
        }

    }

    /*void ControlPosition()
    {
        CharacterController charaController = GetComponent<CharacterController>();
        switch (enemyState)
        {
            case EnemyState.Walk:
                charaController.SimpleMove(transform.forward * Time.deltaTime * walkSpeed);
                break;
            case EnemyState.Run:
                charaController.SimpleMove(transform.forward * Time.deltaTime * runSpeed);
                break;
        }
    }
    */
    void ControlRotation()
    {
        switch (enemyState)
        {
            case EnemyState.TurnAround:
                transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * rotateSpeed);
                break;
            case EnemyState.Run:

            case EnemyState.Attack:
                Vector3 center = new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z);
                transform.LookAt(center);
                break;
        }
    }

    /*Quaternion ThinkOfRotation()
    {
        RotationMap map = GetComponent<RotationMap>();
        List<int> array = map.GetArrayFromMap();

        int index = array[Random.Range(0, array.Count)];
        map.SetPositionToNext(index);

        Vector3 result = Vector3.zero;
        result.y += index * RotateAngle;
        return Quaternion.Euler(result);
    }
    */

    public void WeaponEnable()
    {

       weaponColliderL.enabled = true;
       weaponColliderR.enabled = true;
        
    }
    public void WeaponDisable()
    {

        weaponColliderL.enabled = false;
        weaponColliderR.enabled = false;

    }


    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PlayerWeapon")
        {
            GetDamage(sm.am.attackDamage);
        }
    }

    public void GetDamage(float damage)
    {
        if (sm.isCounterBackSuccess)
        {
        
          Stunned();
          
        }

        animator.SetTrigger("hit");
        health -= damage;
       
        if (health <= 0)
        {
            isDie = true;
            enemyState = EnemyState.Dead;
            animator.SetTrigger("die");
            animator.SetFloat("walk", 0);
            Destroy(co);
            slider.gameObject.SetActive(false);
            //transform.position = new Vector3(transform.position.x, -1, transform.position.z);
        }
    }

    public void Stunned()
    {
 
        animator.SetTrigger("stunned");

    }
}
