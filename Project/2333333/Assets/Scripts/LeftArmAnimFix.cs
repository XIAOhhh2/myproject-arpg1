using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftArmAnimFix : MonoBehaviour
{
    private Animator anim;
    private ActorController ac;
    public Vector3 a;
         
    public void Awake()
    {
        anim = GetComponent<Animator>();
        ac = GetComponentInParent <ActorController>();
    }

    public void OnAnimatorIK(int layerIndex)
    {
        if (ac.leftIsShu)
        {
            if (anim.GetBool("kunai") == false)
            {
                Transform leftUpperArm = anim.GetBoneTransform(HumanBodyBones.LeftUpperArm);
                leftUpperArm.localEulerAngles += a;
                anim.SetBoneLocalRotation(HumanBodyBones.LeftUpperArm, Quaternion.Euler(leftUpperArm.localEulerAngles));

            }

        }

    }
}
