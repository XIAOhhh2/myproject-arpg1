using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class BattleContinue : MonoBehaviour
{

    public Button back;
    public Button exit;
    public Button reset;
    public GameObject p;
    private KeyInPut kp;
    public GameObject ms;
 

    private void Awake()
    {
        kp = p.GetComponent<KeyInPut>();
    }

    public void Continue()
    {
        back.gameObject.SetActive(false);
        exit.gameObject.SetActive(false);
        reset.gameObject.SetActive(false);
        ms.SetActive(false);

        Cursor.visible = false;
        kp.mouseEnable = true;

    }
}