using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraController : MonoBehaviour
{
    private IUserInput pi;
    public float horizontalSpeed = 100.0f;
    public float verticalSpeed = 80.0f;
    public float cameraDampVelue = 0.8f;
    public Image lockDot;
    public bool lockState;

    private GameObject playerHandle;
    private GameObject cameraHandle;
    private float EnlerX;
    private GameObject model;
    private GameObject  newCamera;

    private Vector3 cameraVelocity;
    private LockTarget lockTarget;

    private ActorController ac;
    private EnemyController ec;
    private BossController bc;

    // Start is called before the first frame update
    void Start()
    {
        
        cameraHandle = transform.parent.gameObject;
        playerHandle = cameraHandle.transform.parent.gameObject;
        EnlerX = 20;
        ac = playerHandle.GetComponent<ActorController>();
        model = ac.model;
        pi = ac.pi;

    
        newCamera = Camera.main.gameObject;
        lockDot.enabled = false;
        Cursor.lockState = CursorLockMode.Locked;
     
    }

    // Update is called once per frame
    void /*Fixed*/Update()
    {
        if(lockTarget == null)
        {
            Vector3 ModelEuler = model.transform.eulerAngles;

            playerHandle.transform.Rotate(Vector3.up, pi.Jright * horizontalSpeed * Time./*fixed*/deltaTime);
            EnlerX -= pi.Jup * verticalSpeed * Time.fixedDeltaTime;
            EnlerX = Mathf.Clamp(EnlerX, -30, 45);
            cameraHandle.transform.localEulerAngles = new Vector3(EnlerX, 0, 0);

            model.transform.eulerAngles = ModelEuler;
        }
        else
        {
            Vector3 tempForward = lockTarget.target.transform.position - model.transform.position;
            tempForward.y = 0;
            playerHandle.transform.forward = tempForward;
            cameraHandle.transform.LookAt(lockTarget.target.transform);

            
            lockDot.rectTransform.position = Camera.main.WorldToScreenPoint(lockTarget.target.transform.position + new Vector3(0, lockTarget.halfHeight , 0));

            if(lockTarget.target.tag == "Boss")
            {
               bc = lockTarget.target.GetComponent<BossController>();
            }
            else if (lockTarget.target.tag == "Enemy")
            {
               ec = lockTarget.target.GetComponent<EnemyController>();

            }
            if ((Vector3.Distance(model.transform.position, lockTarget.target.transform.position) > 15.0f) || (lockTarget.target.tag == "Boss")? bc.isDie :ec.isDie)
            {
                LockPr(null, false, false);
            }


            /*if (lockTarget.am != null && lockTarget.am.sm.isDie)
            {
                LockPr(null, false, false);
            }*/
        }

    }
    private void LockPr(LockTarget _lockTarget, bool _lockDotEnable, bool _lockState )
    {
        lockTarget = _lockTarget;
  
        lockDot.enabled = _lockDotEnable;
        
        lockState = _lockState;
    }

    private void FixedUpdate()
    {
    
            //newCamera.transform.position = Vector3.Lerp(newCamera.transform.position, transform.position, cameraDampVelue);
            newCamera.transform.position = Vector3.SmoothDamp(newCamera.transform.position, transform.position, ref cameraVelocity, cameraDampVelue);
            //newCamera.transform.eulerAngles = transform.eulerAngles;
            newCamera.transform.LookAt(cameraHandle.transform);
 
    }

    public void LockUnlock()
    {
        Vector3 modelOrigin1 = model.transform.position;
        Vector3 modelOrigin2 = modelOrigin1 + new Vector3(0, 1, 0);
        Vector3 boxCenter = modelOrigin2 + model.transform.forward * 5.0f;

        Collider[] cols = Physics.OverlapBox(boxCenter, new Vector3(5f, 1f, 5f), model.transform.rotation, LayerMask.GetMask( "Enemy"));
       
         if (cols.Length == 0)
        {
            LockPr(null, false, false);
        }
        else
        {
            foreach (var col in cols)
            {
                if ( lockTarget != null && lockTarget.target == col.gameObject)
                {
                    LockPr(null, false, false);

                    break;
                }
                LockPr(new LockTarget(col.gameObject, col.bounds.extents.y), true, true);
                break;
            }
        }
    } 

    private class LockTarget
    {   public GameObject target;
        public float halfHeight;
        public ActorManager am;
        public LockTarget (GameObject _target, float _halfHeight)
        {
            target = _target;
            halfHeight = _halfHeight;
            am = _target.GetComponent<ActorManager>();
        }
    }


}
