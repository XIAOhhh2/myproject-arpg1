using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MouseSensitivity : MonoBehaviour
{
    public Slider msX;
    public Slider msY;

    public GameObject player;
    private KeyInPut kp;

    private void Awake()
    {
        kp = player.GetComponent<KeyInPut>();
    }
    private void Update()
    {
        kp.mouseSensitivityX = msX.value;
        kp.mouseSensitivityY = msY.value;
    }
}
