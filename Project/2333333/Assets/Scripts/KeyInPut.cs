using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;

public class KeyInPut : IUserInput
{
    [Header("===== Key settings =====")]
    public string keyUp = "w";
    public string keyDown = "s";
    public string keyLeft ="a";
    public string keyRight ="d";

    public string keyA;
    public string keyB;
    public string keyC;
    public string keyD;
    public string keyE;
    public string keyF;

    public string keyJUp;
    public string keyJDown;
    public string keyJLeft;
    public string keyJRight;

    public bool mouseEnable = false;
    public float mouseSensitivityX = 1.0f;
    public float mouseSensitivityY = 1.0f;

    public PlayerButton playerkeyA = new PlayerButton();
    public PlayerButton playerkeyB = new PlayerButton();
    public PlayerButton playerkeyC = new PlayerButton();
    public PlayerButton playerkeyD = new PlayerButton();
    public PlayerButton playerkeyE = new PlayerButton();
    public PlayerButton playerkeyF = new PlayerButton();



  /*  [Header("===== Output signals =====")]    //IUerInput
    public float Dup;
    public float Dright;
    public float Dmag;
    public Vector3 Dvec;
    public float Jup;
    public float Jright;

    public bool run;
    public bool jump;
    private bool lastJump;
    public bool attack;
    private bool lastAttack;

    public bool inputEnabled = true;

    private float targetDup;
    private float targetDright;
    private float velocityDup;
    private float velocityDright;
    */

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        playerkeyA.Tick (Input.GetKey( keyA));
        playerkeyB.Tick (Input.GetKey( keyB));
        playerkeyC.Tick (Input.GetKey( keyC));
        playerkeyD.Tick (Input.GetKey( keyD));
        playerkeyE.Tick (Input.GetKey( keyE));
        playerkeyF.Tick (Input.GetKey( keyF));


        if (mouseEnable == true)
        {
            Jup = Input.GetAxis("Mouse Y") * 2 *mouseSensitivityY;
            Jright = Input.GetAxis("Mouse X") * 12 * mouseSensitivityX;
        }
        else
        {
            Jup = (Input.GetKey(keyJUp) ? 1.0f : 0) - (Input.GetKey(keyJDown) ? 1.0f : 0);
            Jright = (Input.GetKey(keyJRight) ? 1.0f : 0) - (Input.GetKey(keyJLeft) ? 1.0f : 0);

        }





        targetDup = (Input.GetKey(keyUp) ? 1.0f : 0) - (Input.GetKey(keyDown) ? 1.0f : 0);
        targetDright = (Input.GetKey(keyRight) ? 1.0f : 0) - (Input.GetKey(keyLeft) ? 1.0f : 0);

        if (inputEnabled == false)
        {
            targetDup = 0;
            targetDright = 0;
        }

        Dup = Mathf.SmoothDamp(Dup, targetDup, ref velocityDup, 0.1f);
        Dright = Mathf.SmoothDamp(Dright, targetDright, ref velocityDright, 0.1f);

        Vector2 tempDAxis = SquareToCircle(new Vector2(Dright, Dup));
        float Dright2 = tempDAxis.x;
        float Dup2 = tempDAxis.y;

        DmagDvec(Dup2, Dright2);
        //Dmag = Mathf.Sqrt((Dup2 * Dup2) + (Dright2 * Dright2));
        //Dvec = Dright2 * transform.right + Dup2 * transform.forward;

        run = (playerkeyA.IsPressing && !playerkeyA.IsDelaying) || playerkeyA.IsExtending;
        kunai = playerkeyD.IsPressing;
        roll = playerkeyA.OnReleased && playerkeyA.IsDelaying ;
        lockon = playerkeyE.OnPressed;
        

        /* bool newJump = Input.GetKey(keyB);
         if (newJump != lastJump && newJump == true)
         {
             jump = true;
         }
           else
         {
             jump = false;
         }
         lastJump = newJump;
         */
        jump = playerkeyA.OnPressed && playerkeyA.IsExtending;

        /*bool newAttack = Input.GetKey(keyC);
        if (newAttack != lastAttack && newAttack == true)
        {
            attack = true;
        }
        else
        {
            attack = false;
        }      
        lastAttack = newAttack;
        */
        mouse0A = playerkeyC.IsPressing && !playerkeyC.IsDelaying;
        counterBack = playerkeyF.OnReleased && playerkeyF.IsDelaying;
        runAttack = playerkeyB.OnReleased && playerkeyB.IsDelaying;
        mouse0 = playerkeyC.OnReleased && playerkeyC.IsDelaying;
        mouse1 = playerkeyD.OnReleased && playerkeyD.IsDelaying;
    }

   /* private Vector2 SquareToCircle (Vector2 input)
    {
        Vector2 output = Vector2.zero;

        output.x = input.x * Mathf.Sqrt(1 - (input.y * input.y) / 2.0f);
        output.y = input.y * Mathf.Sqrt(1 - (input.x * input.x) / 2.0f);

        return output;
    }
     */
}
